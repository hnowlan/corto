

all:
	go build -o . ./...

clean:
	@rm -f corto

test:
	go test -v ./...


.PHONY: all test
