package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConfig(t *testing.T) {
	c, e := ReadConfig("../config.yaml.sample")

	require.NoError(t, e)

	assert.NotEmpty(t, c.GoogleDriveId)
	assert.NotEmpty(t, c.GoogleDocTemplateId)
}
