package v1

import "time"

type status int

const (
	Open status = iota
	Closed
)

type Incident struct {
	Id         string    // incident ID (i.e. the task ID)
	Started_at time.Time // incident start time (i.e. task open)
	Ended_at   time.Time // when the incident was closed (task closed)
	Title      string    // self explanatory
	Status     status    // the incident status (open, closed, etc)
	Public     bool      // whether the incident is public (i.e. the task itself is public)
}
